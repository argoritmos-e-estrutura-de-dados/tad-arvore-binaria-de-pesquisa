package BinarySearchTree;

import java.util.Random;
/** @author messiah */
public class main {
    public static void main(String[] args) {
        /** Para cada valor de caso de teste (1.000, 2.000, 3.000, 4.000, 5.000, 6.000, 7.000,
         * 8.000, 9.000) foi instanciada um novo objeto BinarySearchTree.
         */
        for(int k = 0; k < 10; k++){
            System.out.println("\n\nCASO DE TESTE: "+k+"\n");
            //Gerar árvores a partir de n elementos ORDENADOS.
            System.out.println("Gerar árvores a partir de n elementos ORDENADOS:");
            for(int n = 1000; n <= 9000; n += 1000){
                BinarySearchTree bt = new BinarySearchTree();
                for(int i = 0; i < n; i++) bt.insert(i);
                long start_time = System.nanoTime();                //Tempo inicial.
                System.out.println("\tNúmero de comparações: "+bt.search(n)+" Com n:"+(n));
                System.out.println("\tTempo de Pesquisa estimado: "+(System.nanoTime()-start_time)+" ns"); //Tempo estimado para pesquisa.
            }

            //Gerar árvores a partir de n elementos ALEATÓRIOS.
            System.out.println("Gerar árvores a partir de n elemento ALEATÓRIOS:");
            for(int n = 1000; n <= 9000; n += 1000){
                BinarySearchTree bt = new BinarySearchTree();
                for(int i = 0; i < n; i++) bt.insert((new Random()).nextInt(n));
                //Tempo inicial para a pesquisa.
                long start_time = System.nanoTime();
                System.out.println("\tNúmero de comparações: "+bt.search(n)+" Com n:"+n);
                //Tempo estimado para pesquisa.
                System.out.println("\tTempo de Pesquisa estimado: "+(System.nanoTime()-start_time)+" ns");
            }
        }
    }    
}