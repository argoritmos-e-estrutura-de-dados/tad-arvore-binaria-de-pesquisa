package BinarySearchTree;

import Item.Item;
/** @author messiah */
public class BinarySearchTree {
    public class Node {
        /** Tipo Item genérico. */
        public Item it;
        /** Apontadores para os filhos à esquerda e à direita do nó. */
        public Node left, right;
        /** Construtor padrão da classe Node.
         * @param it item genérico
         */
        public Node(Item it) {
            this.it = it;
            this.left = this.right = null;
        }      
    }
    /** Nó raiz da árvore binária. */
    private Node root;
    /** Número de comparações na pesquisa. */
    private int number_of_comparisons;
    /** Construtor padrão da classe BinaryTree; neste a raíz recebe o valor null,
     * para afirmar sua inicialização vazia.
     */
    public BinarySearchTree(){ this.root = null; }  
    /** Chamada publica ao método insert(Item it, Node n).
     * @param i conteudo do elemento a ser inserido
     */
    public void insert(int i){ this.root = this.insert(new Item(i), this.root); }
    /** Chamada publica ao método search(Item it, Node n, int number_of_comparisons).
     * @param i conteudo do elemento a ser procurado
     * @return número de comparações realizadas na busca do elemento i
     */
    public int search(int i){
        this.number_of_comparisons = 0;
        if(this.search(new Item(i), this.root)) System.out.println("Item encontrado.");
        return this.number_of_comparisons;
    }
    /** Chamada publica ao método remove(Item it, Node n).
     * @param i conteúdo elemento a ser removido
     */
    public void remove(int i){ this.root = this.remove(new Item(i),this.root); }
    /** Chamada publica ao método print(Node n). */
    public void print(){ this.print(this.root); }
    /** A inserção inicia-se pela busca de um valor contido na árvore que seja igual
     * ao do novo elemento a ser inserido, percorrendo as sub-árvores a esquerda e
     * a direita a partir do nó raiz (Node root), por meio das chamadas recursivas;
     * Se a árvore estiver vazia ou o nó não existir, cria-se um novo nó em que 
     * será inserido as informações.
     * @param it nova chave a ser inserida
     * @param n árvore da recursão
     * @return árvore da recursão
     */
    private Node insert(Item it, Node n){
        if(n == null) n = new Node(it);
        //Se a chave it é menor que n.it, busca-se na sub-árvore esquerda.
        else if(it.compare(n.it) < 0) n.left = this.insert(it, n.left);
        //Se a chave it é maior que n.it, busca-se na sub-árvore direita.
        else if(it.compare(n.it) > 0) n.right = this.insert(it, n.right);
        return n;
    }
    /** A busca ocorre percorrendo-se as sub-árvores a esquerda e a direita da 
     * raíz, por meio das chamadas recursivas até alcançar-se o nó folha da árvore,
     * onde poderá ou não conter o valor requerido. 
     * @param it chave pesquisada
     * @param n árvore da recursão
     * @return 
     */
    private boolean search(Item it, Node n){
        if(n == null) return false;
        //Se a chave it é menor que n.it, busca-se na sub-árvore esquerda, somando +1 ao número de comparações realizadas.
        else if(it.compare(n.it) < 0){ this.number_of_comparisons++; return this.search(it, n.left); }
        //Se a chave it é maior que n.it, busca-se na sub-árvore direita, somando +1 ao número de comparações realizadas.
        else if(it.compare(n.it) > 0){ this.number_of_comparisons++; return this.search(it, n.right); }
        return true;
    }
    /** Funçaõ auxiliar para a remoção de elementos da árvore.
     * @param n_1 nó axiliar
     * @param n_2 nó auxiliar
     * @return 
     */
    private Node antecessor(Node n_1, Node n_2){
        if(n_2.right != null) n_2.right = this.antecessor(n_1, n_2.right);
        else {
            n_1.it = n_2.it;
            n_2 = n_2.left;
        }
        return n_2;
    }
    /** A remoção ocorre percorrendo-se as sub-árvores a esquerda e a direita da 
     * raíz, por meio das chamadas recursivas até alcançar-se o nó a ser retirado;
     * Para excluir um nó deve-se considerar três casos distintos:
     * * remoção da folha: basta remove-lo da árvore, exclui o nó folha e retona a raíz atualizada;
     * * remoção de um nó com um filho: exclui o nó a ser retirado, e o filho substitui a posição do pai;
     * * remoção de um nó com dois filhos: substitui-se o valor do nó a ser retirado
     * pelo valor sucessor, no caso o nó mais à esquerda da subárvore a direita.
     * @param it 
     * @param n árvore da recursão
     * @return 
     */
    private Node remove(Item it, Node n){
        if(n == null) System.out.println("Item não existente.");
        //Se a chave it é menor que n.it, busca-se na sub-árvore esquerda.
        else if(it.compare(n.it) < 0) return this.remove(it, n.left);
        //Se a chave it é maior que n.it, busca-se na sub-árvore direita.
        else if(it.compare(n.it) > 0) return this.remove(it, n.right);
        else{
            if(n.right == null) n = n.left;
            else if(n.left == null) n = n.right;
            else n.left = this.antecessor(n, n.left);
        }
        return n;
    }
    /** Função recursiva que percorre a árvore binária da esquerda para a direita
     * imprimindo o conteúdo de cada elemento.
     * @param n árvore da recursão
     */    
    private void print(Node n){
        if(n != null){
            this.print(n.left);
            System.out.print(n.it.getKey() + ";");
            this.print(n.right);
        }
    }
}
